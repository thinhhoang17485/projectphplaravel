<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPubBookIdToPayloadBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payload_book', function (Blueprint $table) {
            $table->unsignedBigInteger('pub_book_id')->after('id');

            $table->foreign('pub_book_id')->references('id')->on('publish_company_book')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payload_book', function (Blueprint $table) {
            //
        });
    }
}
