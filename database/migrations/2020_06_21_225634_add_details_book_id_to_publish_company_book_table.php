<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailsBookIdToPublishCompanyBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publish_company_book', function (Blueprint $table) {
            $table->unsignedBigInteger('details_book_id')->after('publish_company_id');

            $table->foreign('details_book_id')->references('id')->on('details_book')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publish_company_book', function (Blueprint $table) {
            //
        });
    }
}
