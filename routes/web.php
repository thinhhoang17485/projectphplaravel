<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('auth.showViewLogin');
});

Route::namespace('Authentication')->prefix('auth')->name('auth.')->group(function() {
    Route::get('/', 'AuthController@showViewAuth')->name('showViewLogin');
    Route::post('login', 'AuthController@progressLogin')->name('progressLogin');
    Route::post('register', 'AuthController@progressRegister')->name('progressRegister');
    Route::get('logout', 'AuthController@logout')->name('logout');
});

Route::middleware('checkLogin')->group(function() {
    Route::middleware('checkAdmin')->prefix('admin')->namespace('admin')->name('admin.')->group(function() {
        Route::get('home', 'HomeController@index')->name('home');
        Route::prefix('manage')->name('manage.')->group(function() {
            Route::prefix('account')->name('account.')->group(function() {
                Route::get('index', 'AccountController@index')->name('index');
                Route::get('create', 'AccountController@create')->name('create');
                Route::post('store', 'AccountController@store')->name('store');
                Route::get('edit/{id}', 'AccountController@edit')->name('edit');
                Route::post('update/{id}', 'AccountController@update')->name('update');
                Route::get('destroy/{id}', 'AccountController@destroy')->name('destroy');
            });
            Route::prefix('book')->name('book.')->group(function() {
                Route::get('index', 'BookController@index')->name('index');
                Route::get('show/{id}', 'BookController@show')->name('show');
                Route::get('create', 'BookController@create')->name('create');
                Route::prefix('create')->name('create.')->group(function() {
                    Route::get('new', 'BookController@createNew')->name('new');
                    Route::get('existing', 'BookController@createExistingIndex')->name('existingIndex');
                    Route::get('existing/{id}', 'BookController@createExisting')->name('existing');
                });
                Route::prefix('store')->name('store.')->group(function() {
                    Route::post('new', 'BookController@storeNew')->name('new');
                    Route::post('existing/{id}', 'BookController@storeExisting')->name('existing');
                });
                Route::get('edit/{id}', 'BookController@edit')->name('edit');
                Route::post('update/{id}', 'BookController@update')->name('update');
                Route::get('destroy/{id}', 'BookController@destroy')->name('destroy');
                Route::get('destroy-one-version/{id}', 'BookController@destroyOneVersion')->name('destroyOneVersion');
                Route::prefix('child')->namespace('childBook')->name('child.')->group(function() {
                    Route::get('show/{id}/{idToBack}', 'ChildBookController@show')->name('show');
                });
            });
            Route::prefix('category')->name('category.')->group(function() {
                Route::get('index', 'CategoryController@index')->name('index');
                Route::get('create', 'CategoryController@create')->name('create');
                Route::post('store', 'CategoryController@store')->name('store');
                Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
                Route::post('update/{id}', 'CategoryController@update')->name('update');
                Route::get('destroy/{id}', 'CategoryController@destroy')->name('destroy');
            });
            Route::prefix('publish-company')->name('publishCompany.')->group(function() {
                Route::get('index', 'PublishCompanyController@index')->name('index');
                Route::get('create', 'PublishCompanyController@create')->name('create');
                Route::post('store', 'PublishCompanyController@store')->name('store');
                Route::get('edit/{id}', 'PublishCompanyController@edit')->name('edit');
                Route::post('update/{id}', 'PublishCompanyController@update')->name('update');
                Route::get('destroy/{id}', 'PublishCompanyController@destroy')->name('destroy');
            });
            Route::prefix('profile')->name('profile.')->group(function() {
                Route::get('index', 'ProfileController@index')->name('index');
            });
        });
        Route::prefix('others')->name('others.')->group(function() {
            Route::prefix('analysis')->name('analysis.')->group(function() {
                Route::get('index', 'AnalysisController@index')->name('index');
            });
            Route::prefix('history')->name('history.')->group(function() {
                Route::get('admin', 'HistoryController@showHistoryAdmin')->name('admin');
                Route::get('search', 'HistoryController@showHistorySearch')->name('search');
                Route::get('user', 'HistoryController@showHistoryUser')->name('user');
            });
            Route::prefix('report')->name('report.')->group(function() {
                Route::get('index', 'ReportController@index')->name('index');
            });
            Route::prefix('request')->name('request.')->group(function() {
                Route::get('index', 'RequestController@index')->name('index');
            });
            Route::prefix('statistical')->name('statistical.')->group(function() {
                Route::get('index', 'StatisticalController@index')->name('index');
            });
        });
        Route::get('setting', 'SettingController@index')->name('setting');
    });
    Route::middleware('checkUser')->prefix('user')->name('user.')->group(function() {
        Route::get('home', function () {
            return view('user/index');
        })->name('home');
    });
});
