<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('category')->orderBy('id', 'DESC')->get();
        return view('admin/modules/management/category/index')->with(['categories' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/modules/management/category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = request()->validate([
            'name' => 'required|unique:category',
        ],[
            'name.required'  => 'Bắt buộc!',
            'name.unique' => 'Đã tồn tại!',
        ]);
        $data = $request->except('_token');
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;

        DB::table('category')->insert($data);
        return redirect()->route('admin.manage.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('category')->where('id', $id)->first();
        return view('admin/modules/management/category/edit')->with(['category' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = request()->validate([
            'name' => 'required',
        ],[
            'name.required'  => 'Bắt buộc!',
        ]);
        $currentName = DB::table('category')->where('id', $id)->value('name');
        if ($request['name'] != $currentName
            && DB::table('category')->where('name', $request['name'])->first() != null) {
            return redirect()->back()->withErrors(['name' => 'Đã tồn tại']);
        }

        $data = $request->except('_token');
        $data['updated_at'] = new DateTime;
        DB::table('category')->where('id', $id)->update($data);
        return redirect()->route('admin.manage.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('category')->where('id', $id)->delete();

        return redirect()->route('admin.manage.category.index');
    }
}
