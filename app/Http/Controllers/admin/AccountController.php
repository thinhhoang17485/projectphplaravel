<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('users')->orderBy('id', 'DESC')->get();
        $currentEmail = Auth::user()->email;
        return view('admin/modules/management/account/index')->with(['users' => $data, 'currentEmail' => $currentEmail]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/modules/management/account/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = request()->validate([
            'firstname'  => 'required|max:15',
            'lastname'  => 'required|max:15',
            'email' => 'required|unique:users|regex:/^.+@.+$/i',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ],[
            'firstname.max'  => 'Tối đa 15 kí tự',
            'firstname.required'  => 'Bắt buộc!',
            'lastname.max'  => 'Tối đa 15 kí tự',
            'lastname.required'  => 'Bắt buộc!',
            'email.required' => 'Bắt buộc!',
            'email.regex:/^.+@.+$/i' => 'Không đúng quy chuẩn!',
            'email.unique' => 'Email đã tồn tại!',
            'password.required' => 'Bắt buộc!',
            'password.min' => 'Mật khẩu phải dài hơn 6 kí tự.',
            'password.confirmed' => 'Xác nhận lỗi!',
            'password_confirmation.required' => 'Bắt buộc!',
        ]);
        $data = $request;
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;
        $data['password'] = bcrypt($data['password']);
        $data['isAdmin'] = $data['isAdmin'] == 1;
        $data['active'] = $data['active'] == 1;

        $data = $request->except('_token', 'password_confirmation');

        $data = DB::table('users')->insert($data);
        return redirect()->route('admin.manage.account.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('users')->where('id', $id)->first();
        return view('admin/modules/management/account/edit')->with(['user' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        $data['updated_at'] = new DateTime;

        DB::table('users')->where('id', $id)->update($data);
        return redirect()->route('admin.manage.account.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('admin.manage.account.index');
    }
}
