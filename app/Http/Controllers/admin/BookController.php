<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    private function getDetailBookData()
    {
        $data = DB::table('details_book')
            ->join('author_book', 'author_book.details_book_id', '=', 'details_book.id')
            ->join('details_author', 'details_author.id', '=', 'author_book.author_id')
            ->select('details_book.id as idBook',
                'details_book.name as nameBook',
                'details_book.image as imageBook',
                'details_book.description as descriptionBook',
                'details_author.name as nameAuthor',
            )
            ->get();
        return $data;
    }
    private function getDetailBookDataById($id)
    {
        $data = $this->getDetailBookData()->where('idBook', $id)->first();

        return $data;
    }
    private function getCategorySelected($id)
    {
        $rawCategorySelected = DB::table('book_category')->where('book_id', $id)->get();
        $categorySelected = [];
        $i = 0;
        foreach ($rawCategorySelected as $item) {
            $categorySelected[$i++] = $item->category_id;
        }
        return $categorySelected;
    }
    private function isExistingBook($name, $author, $originalId = null)
    {
        $idBook = DB::table('details_book')->where('name', $name)->value('id');
        if ($originalId != null && $originalId == $idBook) {
            return false;
        }
        $idAuthor = DB::table('author_book')->where('details_book_id', $idBook)->value('author_id');
        $nameAuthor = DB::table('details_author')->where('id', $idAuthor)->value('name');
        return $author == $nameAuthor;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->getDetailBookData();

        return view('admin/modules/management/book/index')->with(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/modules/management/book/create/index');
    }
    public function createNew()
    {
        $categories = DB::table('category')->get();
        $publishCompanies = DB::table('publish_company')->get();

        return view('admin/modules/management/book/create/new/create')->with(['categories' => $categories, 'publishCompanies' => $publishCompanies]);
    }
    public function createExistingIndex()
    {
        $data = $this->getDetailBookData();

        return view('admin/modules/management/book/create/existing/index')->with(['data' => $data]);
    }
    public function createExisting($id)
    {
        $data = $this->getDetailBookDataById($id);

        $publishCompanies = DB::table('publish_company')->get();
        $categories = DB::table('category')->get();
        $categorySelected = $this->getCategorySelected($id);

        return view('admin/modules/management/book/create/existing/create')
            ->with(['data' => $data,
                'publishCompanies' => $publishCompanies,
                'categories' => $categories,
                'categorySelected' => $categorySelected
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNew(Request $request)
    {
        $validateData = $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required|max:30',
            'author' => 'required|max:30',
            'parameter' => 'required|max:100',
            'price' => 'required|regex:/^[0-9]+$/',
            'publishCompany' => 'required',
        ],[
            'image.image' => 'Phải là hình ảnh.',
            'image.mimes' => 'Phải là hình ảnh.',
            'image.max' => 'Kích thước quá lớn.',
            'name.required' => 'Bắt buộc!',
            'name.max' => 'Tối đa 30 kí tự',
            'author.required' => 'Bắt buộc!',
            'parameter.max' => 'Tối đa 100 kí tự',
            'parameter.required' => 'Bắt buộc!',
            'author.max' => 'Tối đa 30 kí tự',
            'price.required' => 'Bắt buộc!',
            'price.regex' => 'Sai hình thức.',
            'publishCompany.required' => 'Bắt buộc!',
        ]);
        if ($this->isExistingBook($request->name, $request->author)) {
            return redirect()->back()->withErrors(['existingBook' => 'Sách đã tồn tại!']);
        };

        $data = $request->except('_token');
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;

        $data['categories'] = $request->input('categories');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('imgStorage/book/', $filename);
            $data['image']  = $filename;
        } else {
            $data['image'] = null;
        }
        $boo['name'] = $data['name'];
        $boo['image'] = $data['image'];
        $boo['description'] = $data['description'];
        $boo['created_at'] = $data['created_at'];
        $boo['updated_at'] = $data['updated_at'];
        DB::table('details_book')->insert($boo);

        $idBook = DB::table('details_book')->latest('id')->first()->id;

        if ($data['categories'] != null) {
            foreach ($data['categories'] as $category) {
                $cat['book_id'] = $idBook;
                $cat['category_id'] = $category;
                $cat['created_at'] = $data['created_at'];
                $cat['updated_at'] = $data['updated_at'];
                DB::table('book_category')->insert($cat);
            }
        }

        $pub['publish_company_id'] = $data['publishCompany'];
        $pub['details_book_id'] = $idBook;
        $pub['created_at'] = $data['created_at'];
        $pub['updated_at'] = $data['updated_at'];
        DB::table('publish_company_book')->insert($pub);

        $aut['name'] = $data['author'];
        $idAuthor = DB::table('details_author')->where('name', $aut['name'])->value('id');
        if ($idAuthor == null) {
            $aut['created_at'] = $data['created_at'];
            $aut['updated_at'] = $data['updated_at'];
            DB::table('details_author')->insert($aut);
            $idAuthor = DB::table('details_author')->latest('id')->first()->id;
        }
        $aut_book['author_id'] = $idAuthor;
        $aut_book['details_book_id'] = $idBook;
        $aut_book['created_at'] = $data['created_at'];
        $aut_book['updated_at'] = $data['updated_at'];
        DB::table('author_book')->insert($aut_book);

        $pub_book_id = DB::table('publish_company_book')->latest('id')->first()->id;
        $pri['pub_book_id'] = $pub_book_id;
        $pri['price'] = $data['price'];
        $pri['parameter'] = $data['parameter'];
        $pri['created_at'] = $data['created_at'];
        $pri['updated_at'] = $data['updated_at'];
        DB::table('payload_book')->insert($pri);

        return redirect()->route('admin.manage.book.index');
    }
    public function storeExisting(Request $request, $id)
    {
        $validateData = $request->validate([
            'parameter' => 'required|max:100',
            'price' => 'required|regex:/^[0-9]+$/',
            'publishCompany' => 'required',
        ],[
            'parameter.max' => 'Tối đa 100 kí tự',
            'parameter.required' => 'Bắt buộc!',
            'price.required' => 'Bắt buộc!',
            'price.regex' => 'Sai hình thức.',
            'publishCompany.required' => 'Bắt buộc!',
        ]);

        $data = $request->except('_token');
        $pub_book['publish_company_id'] = $data['publishCompany'];
        $pub_book['details_book_id'] = $id;
        DB::table('publish_company_book')->insert($pub_book);

        $idPubBoo = DB::table('publish_company_book')->latest('id')->first()->id;
        $price['pub_book_id'] = $idPubBoo;
        $price['price'] = $data['price'];
        $price['parameter'] = $data['parameter'];
        DB::table('payload_book')->insert($price);

        return redirect()->route('admin.manage.book.show', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = $this->getDetailBookDataById($id);

        $categories = DB::table('category')->get();
        $categorySelected = $this->getCategorySelected($id);

        $childs = DB::table('publish_company_book')
            ->join('publish_company', 'publish_company.id', '=', 'publish_company_book.publish_company_id')
            ->join('payload_book', 'publish_company_book.id', '=', 'payload_book.pub_book_id')
            ->where('publish_company_book.details_book_id', $id)
            ->select('publish_company_book.id as idPubBoo',
                'publish_company.name as namePublishCompany',
                'payload_book.parameter as parameterBook',
                'payload_book.price as priceBook',
            )
            ->get();

        return view('admin/modules/management/book/show')
            ->with(['book' => $book,
                'categories' => $categories,
                'categorySelected' => $categorySelected,
                'childs' => $childs,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->getDetailBookDataById($id);

        $categories = DB::table('category')->get();
        $categorySelected = $this->getCategorySelected($id);

        return view('admin/modules/management/book/edit')
            ->with(['data' => $data,
                'categories' => $categories,
                'categorySelected' => $categorySelected,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required|max:30',
            'author' => 'required|max:30',
        ],[
            'image.image' => 'Phải là hình ảnh.',
            'image.mimes' => 'Phải là hình ảnh.',
            'image.max' => 'Kích thước quá lớn.',
            'name.required' => 'Bắt buộc!',
            'name.max' => 'Tối đa 30 kí tự',
            'author.required' => 'Bắt buộc!',
            'author.max' => 'Tối đa 30 kí tự',
        ]);

        $data = $request->except('_token');
        $data['updated_at'] = new DateTime;

        $data['categories'] = $request->input('categories');

        $boo = [];
        if ($request->hasFile('image')) {
            $oldImage = DB::table('details_book')->where('id', $id)->value('image');
            if ($oldImage != null) {
                try {
                    $oldImage = 'imgStorage/book/' . $oldImage;
                    unlink($oldImage);
                } catch(\Exception $e) {}
            }

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('imgStorage/book/', $filename);
            $data['image']  = $filename;
            $boo['image'] = $data['image'];
        }
        $boo['name'] = $data['name'];
        $boo['description'] = $data['description'];
        $boo['updated_at'] = $data['updated_at'];
        DB::table('details_book')->where('id', $id)->update($boo);

        DB::table('book_category')->where('book_id', $id)->delete();
        if ($data['categories'] != null) {
            foreach ($data['categories'] as $category) {
                $cat['book_id'] = $id;
                $cat['category_id'] = $category;
                $cat['updated_at'] = $data['updated_at'];
                DB::table('book_category')->insert($cat);
            }
        }

        $aut['name'] = $data['author'];
        $idAuthor = DB::table('details_author')->where('name', $aut['name'])->value('id');
        if ($idAuthor == null) {
            $aut['created_at'] = $data['created_at'];
            $aut['updated_at'] = $data['updated_at'];
            DB::table('details_author')->insert($aut);
            $idAuthor = DB::table('details_author')->latest('id')->first()->id;
        }
        $aut_book['author_id'] = $idAuthor;
        $aut_book['updated_at'] = $data['updated_at'];
        DB::table('author_book')->where('details_book_id', $id)->update($aut_book);

        return redirect()->route('admin.manage.book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $idBook = DB::table('publish_company_book')->where('id', $id)->value('details_book_id');
            $oldImage = 'imgStorage/book/' . (DB::table('details_book')->where('id', $idBook)->value('image'));
            unlink($oldImage);
        } catch(\Exception $e) {}
        DB::table('details_book')->where('id', $idBook)->delete();

        return redirect()->route('admin.manage.book.index');
    }
    public function destroyOneVersion($id)
    {
        DB::table('publish_company_book')->where('id', $id)->delete();

        return redirect()->back();
    }
}

