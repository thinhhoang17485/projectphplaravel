<?php

namespace App\Http\Controllers\admin\childBook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ChildBookController extends Controller
{
    private function getDetailBookData()
    {
        $data = DB::table('details_book')
            ->join('author_book', 'author_book.details_book_id', '=', 'details_book.id')
            ->join('details_author', 'details_author.id', '=', 'author_book.author_id')
            ->select('details_book.id as idBook',
                'details_book.name as nameBook',
                'details_book.image as imageBook',
                'details_book.description as descriptionBook',
                'details_author.name as nameAuthor',
            )
            ->get();
        return $data;
    }
    private function getDetailBookDataById($id)
    {
        $data = $this->getDetailBookData()->where('idBook', $id)->first();

        return $data;
    }
    private function getPayloadBookDataById($id)
    {
        $data = DB::table('publish_company_book')
            ->join('publish_company', 'publish_company.id', '=', 'publish_company_book.publish_company_id')
            ->join('payload_book', 'payload_book.pub_book_id', '=', 'publish_company_book.id')
            ->where('publish_company_book.id', $id)
            ->select('publish_company_book.id as idPubBoo',
                'publish_company.name as namePublishCompany',
                'payload_book.price as priceBook',
                'payload_book.parameter as parameterBook',
            )
            ->get()->first();
        return $data;
    }
    private function getCategorySelected($id)
    {
        $rawCategorySelected = DB::table('book_category')->where('book_id', $id)->get();
        $categorySelected = [];
        $i = 0;
        foreach ($rawCategorySelected as $item) {
            $categorySelected[$i++] = $item->category_id;
        }
        return $categorySelected;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $idToBack)
    {
        $idBook = DB::table('publish_company_book')
            ->where('id', $id)
            ->value('details_book_id');
        $book = $this->getDetailBookDataById($idBook);
        $payload = $this->getPayloadBookDataById($id);

        $categories = DB::table('category')->get();
        $categorySelected = $this->getCategorySelected($idBook);

        // dd($payload);
        return view('admin/modules/management/book/child/show')
            ->with(['book' => $book,
                'payload' => $payload,
                'categories' => $categories,
                'categorySelected' => $categorySelected,
            ]);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
