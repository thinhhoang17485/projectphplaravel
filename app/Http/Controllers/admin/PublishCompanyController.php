<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;

class PublishCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('publish_company')->orderBy('id', 'DESC')->get();
        $rawConstraint = DB::table('publish_company_book')->select('publish_company_id')->distinct()->get();
        $constraint = [];

        $i = 0;
        foreach ($rawConstraint as $item) {
            $constraint[$i++] = $item->publish_company_id;
        }
        return view('admin/modules/management/publishCompany/index')->with(['publishCompanies' => $data, 'constraint' => $constraint]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/modules/management/publishCompany/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = request()->validate([
            'name' => 'required|unique:publish_company',
        ],[
            'name.required'  => 'Bắt buộc!',
            'name.unique' => 'Đã tồn tại!',
        ]);
        $data = $request->except('_token');
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;

        $data = DB::table('publish_company')->insert($data);
        return redirect()->route('admin.manage.publishCompany.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('publish_company')->where('id', $id)->first();
        return view('admin/modules/management/publishCompany/edit')->with(['publishCompany' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = request()->validate([
            'name' => 'required',
        ],[
            'name.required'  => 'Bắt buộc!',
        ]);
        $currentName = DB::table('publish_company')->where('id', $id)->value('name');
        if ($request['name'] != $currentName
            && DB::table('publish_company')->where('name', $request['name'])->first() != null) {
            return redirect()->back()->withErrors(['name' => 'Đã tồn tại']);
        }

        $data = $request->except('_token');
        $data['updated_at'] = new DateTime;

        DB::table('publish_company')->where('id', $id)->update($data);
        return redirect()->route('admin.manage.publishCompany.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('publish_company')->where('id', $id)->delete();
        return redirect()->route('admin.manage.publishCompany.index');
    }
}
