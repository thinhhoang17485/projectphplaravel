<?php

namespace App\Http\Controllers\authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DateTime;

class AuthController extends Controller
{
    public function showViewAuth() {
        return view('authentication/modules/auth');
    }
    public function logout() {
        Auth::logout();
        return redirect()->route('auth.showViewLogin');
    }
    public function progressLogin(Request $request) {
        $validateData = request()->validate([
            'emailL' => 'required|regex:/^.+@.+$/i',
            'passwordL' => 'required|min:6',
        ],[
            'emailL.required' => 'Bắt buộc!',
            'emailL.regex:/^.+@.+$/i' => 'Không đúng quy chuẩn!',
            'passwordL.required' => 'Bắt buộc!',
            'passwordL.min' => 'Mật khẩu phải dài hơn 6 kí tự.',
        ]);

        $data['email'] = $request['emailL'];
        $data['password'] = $request['passwordL'];
        $credentials = $data;

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if (!$user->active) {
                Auth::logout();
                return redirect()->back()->withErrors(['alert' => 'Tài khoản bị khóa!']);
            }
            return redirect()->route(($user->isAdmin) ? 'admin.home' : 'user.home');
        } else {
            return redirect()->back()->withErrors(['alert' => 'Email hoặc mật khẩu không đúng!']);
        }
    }
    public function progressRegister(Request $request) {
        $validateData = request()->validate([
            'firstname'  => 'required|max:15',
            'lastname'  => 'required|max:15',
            'email' => 'required|unique:users|regex:/^.+@.+$/i',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ],[
            'firstname.max'  => 'Tối đa 15 kí tự',
            'firstname.required'  => 'Bắt buộc!',
            'lastname.max'  => 'Tối đa 15 kí tự',
            'lastname.required'  => 'Bắt buộc!',
            'email.required' => 'Bắt buộc!',
            'email.regex:/^.+@.+$/i' => 'Không đúng quy chuẩn!',
            'email.unique' => 'Email đã tồn tại!',
            'password.required' => 'Bắt buộc!',
            'password.min' => 'Mật khẩu phải dài hơn 6 kí tự.',
            'password.confirmed' => 'Xác nhận lỗi!',
            'password_confirmation.required' => 'Bắt buộc!',
        ]);

        $checkAdmin = DB::table('users')->value('email');

        $data = $request;
        $data['isAdmin'] = $checkAdmin == null;
        $data['active'] = true;
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;
        $data['password'] = bcrypt($data['password']);

        $data = $request->except('_token', 'password_confirmation');

        $data = DB::table('users')->insert($data);
        return redirect()->back()->withErrors(['alert' => 'Đăng kí thành công']);
    }
}
