<!DOCTYPE html>
<html lang="en">
<head>
    @include('authentication/blocks/head')
</head>
<body>
    <div class="login-wrap">
        <div class="login-html">
            @yield('content')
        </div>
    </div>
</body>
<footer>
    @include('authentication/blocks/footer')
</footer>
</html>
