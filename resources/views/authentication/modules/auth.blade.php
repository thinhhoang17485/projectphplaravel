@extends('authentication/master')
@section('title', 'Đăng nhập - Đăng kí')

@section('content')
@if (!$errors->any() || $errors->has('emailL') || $errors->has('passwordL') || $errors->has('alert'))
    <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Đăng nhập</label>
    <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Đăng kí</label>
@else
    <input id="tab-1" type="radio" name="tab" class="sign-in"><label for="tab-1" class="tab">Đăng nhập</label>
    <input id="tab-2" type="radio" name="tab" class="sign-up" checked><label for="tab-2" class="tab">Đăng kí</label>
@endif
<div class="login-form">
    <div id="login" class="col s12 sign-in-htm">
        <form class="col s12" method="POST" action="{{ route('auth.progressLogin') }}">
            @csrf
            <div class="form-container">
                <h4 class="teal-text">Xin chào</h4>
                @if ($errors->has('emailL'))
                    <strong>{{ $errors->first('emailL') }}</strong>
                @endif
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate text-color" name="emailL" value="{{ old('emailL') }}">
                        <label for="email">Email</label>
                    </div>
                </div>
                @if ($errors->has('passwordL'))
                    <strong>{{ $errors->first('passwordL') }}</strong>
                @endif
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="validate text-color" name="passwordL">
                        <label for="password">Mật khẩu</label>
                    </div>
                </div>
                @if ($errors->has('alert'))
                    <div class="input-field col s12">
                        <strong>{{ $errors->first('alert') }}</strong>
                    </div>
                @endif
                <br>
                <center>
                    <button class="btn waves-effect waves-light teal" type="submit">Đăng nhập</button>
                    <br>
                    <br>
                    <a href="">Quên mật khẩu?</a>
                </center>
            </div>
        </form>
    </div>
    <div id="register" class="col s12 sign-up-htm">
        <form class="col s12" method="POST" action="{{ route('auth.progressRegister') }}">
            @csrf
            <div class="form-container">
                <h5 class="teal-text">Thành viên mới</h5>
                @if ($errors->has('lastname') || $errors->has('firstname'))
                    <strong>Họ tên bắt buộc!</strong>
                @endif
                <div class="row">
                    <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate text-color" name="lastname" value="{{ old('lastname') }}">
                        <label for="last_name">Họ</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate text-color" name="firstname" value="{{ old('firstname') }}">
                        <label for="last_name">Tên</label>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <strong>{{ $errors->first('email') }}</strong>
                @endif
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate text-color" name="email" value="{{ old('email') }}">
                        <label for="email">Email</label>
                    </div>
                </div>
                @if ($errors->has('password'))
                    <strong>{{ $errors->first('password') }}</strong>
                @endif
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="validate text-color" name="password">
                        <label for="password">Mật khẩu</label>
                    </div>
                </div>
                @if ($errors->has('password_confirmation'))
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                @endif
                <div class="row input-field col s12">
                    <div class="input-field col s12">
                        <input id="password-confirm" type="password" class="validate text-color" name="password_confirmation">
                        <label for="password-confirm">Xác nhận mật khẩu</label>
                    </div>
                </div>
                <center>
                    <button class="btn waves-effect waves-light teal" type="submit">Đăng kí</button>
                </center>
            </div>
        </form>
    </div>
</div>
@endsection
