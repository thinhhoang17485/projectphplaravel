<!DOCTYPE html>
<head>
    @include('admin/blocks/head')
</head>
<html>
   <body class="hold-transition sidebar-mini sidebar-collapse">
      <!-- Site wrapper -->
      <div class="wrapper">
         <!-- Navbar -->
         @include('admin/blocks/navbar')
         <!-- Main Sidebar Container -->
         @include('admin/blocks/asidebar')
         <!-- Content Wrapper. Contains page content -->
         @include('admin/blocks/content')
         <!-- /.content-wrapper -->
         @include('admin/blocks/footer')
      </div>
      <!-- ./wrapper -->
   </body>
   @include('admin/blocks/foot')
</html>
