@extends('admin/master')
@section('title', 'Nhà xuất bản')
@section('taskname', 'Danh sách')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonPublishComSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid col-10">
        <div class="row">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tên</th>
                        <th>Mô tả</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($publishCompanies as $publishCom)
                    <tr>
                        <td>{{ $publishCom->name }}</td>
                        <td>{{ $publishCom->description }}</td>
                        <td>
                            <a href="{{ route('admin.manage.publishCompany.edit', ['id' => $publishCom->id]) }}">
                                <button type="button" class="btn btn-block btn-outline-secondary btn-lg">Sửa</button>
                            </a>
                        </td>
                        <td>
                            <a onclick="return alert('Bạn có chắc muốn xóa không?');" href="{{ route('admin.manage.publishCompany.destroy', ['id' => $publishCom->id]) }}">
                                <button type="button" class="btn btn-block btn-outline-danger btn-lg
                                    {{ in_array($publishCom->id, $constraint) ? 'disabled' : ''}}">
                                    Xóa
                                </button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Tên</th>
                        <th>Mô tả</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
