@extends('admin/master')
@section('title', 'Thể loại')
@section('taskname', 'Thêm')
@section('additionButtonOpen', 'menu-open')
@section('additionButtonSelected', 'active')
@section('additionButtonCategorySelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <form method="POST" action="{{ route('admin.manage.category.store') }}">
    @csrf
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Điền thông tin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label for="inputName">Tên thể loại
                    <span>
                        @if ($errors->has('name'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('name') }}
                        </strong>
                        @endif
                    </span>
                </label>
                <input type="text" id="inputName" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="inputDescription">Mô tả</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description"></textarea>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <div class="col-md-3"></div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
    <div class="col-3"></div>
      <div class="col-6">
        <a href="{{ route('admin.manage.category.index') }}" class="btn btn-secondary">Hủy</a>
        <button class="btn btn-success float-right" type="submit">Thực hiện</button>
      </div>
      <div class="col-3"></div>
    </div>
    </form>
</section>
<!-- /.content -->
@endsection
