@extends('admin/master')
@section('title', 'Tài khoản')
@section('taskname', 'Cấu hình')

@section('content')
<!-- Main content -->
<section class="content">
    <form method="POST" action="{{ route('admin.manage.account.update', ['id' => $user->id]) }}">
    @csrf
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thông tin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputName">Họ <span>
                        @if ($errors->has('lastname'))
                            <strong style="color: red">*</strong>
                        @endif
                        </span>
                    </label>
                    <input value="{{ $user->lastname }}" type="text" id="inputName" name="lastname" class="form-control" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputName">Tên <span>
                        @if ($errors->has('firstname'))
                            <strong style="color: red">*</strong>
                        @endif
                        </span>
                    </label>
                    <input value="{{ $user->firstname }}" type="text" id="inputName" name="firstname" class="form-control" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="inputName">Email  <span>
                    @if ($errors->has('email'))
                        <strong style="color: red">*</strong>
                    @endif
                    </span>
                </label>
                <input value="{{ $user->email }}" type="text" id="inputName" name="email" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label for="inputStatus">Phân quyền</label>
                <select class="form-control custom-select" name="isAdmin">
                  <option value="1" {{ $user->isAdmin ? 'selected' : '' }}">Quản trị</option>
                  <option value="0" {{ !$user->isAdmin ? 'selected' : '' }}>Người dùng</option>
                </select>
              </div>
            <div class="form-group">
              <label for="inputStatus">Trạng thái</label>
              <select class="form-control custom-select" name="active">
                <option value="1" {{ $user->active ? 'selected' : '' }}>Kích hoạt</option>
                <option value="0" {{ !$user->active ? 'selected' : '' }}>Vô hiệu</option>
              </select>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <div class="col-md-3"></div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
    <div class="col-3"></div>
      <div class="col-6">
        <a href="{{ route('admin.manage.account.index') }}" class="btn btn-secondary">Hủy</a>
        <button class="btn btn-success float-right" type="submit">Thực hiện</button>
      </div>
      <div class="col-3"></div>
    </div>
    </form>
</section>
<!-- /.content -->
@endsection
