@extends('admin/master')
@section('title', 'Tài khoản')
@section('taskname', 'Danh sách')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonAccountSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tên</th>
                        <th>Email</th>
                        <th>Phân quyền</th>
                        <th>Trạng thái</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->lastname . " " . $user->firstname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ ($user->isAdmin) ? 'Admin' : 'Người dùng'}}</td>
                            <td>{{ ($user->active) ? 'Kích hoạt' : 'Vô hiệu'}}</td>
                            <td>
                                <a href="{{ route('admin.manage.account.edit', ['id' => $user->id]) }}"><button type="button" class="btn btn-block btn-outline-secondary btn-lg" {{ ($user->email == $currentEmail) ? 'disabled' : '' }}>Cấu hình</button></a>
                            </td>
                            <td>
                                <a onclick="return alert('Bạn có chắc muốn xóa không?');" href="{{ route('admin.manage.account.destroy', ['id' => $user->id]) }}"><button type="button" class="btn btn-block btn-outline-danger btn-lg" {{ ($user->email == $currentEmail) ? 'disabled' : '' }}>Xóa</button></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Tên</th>
                        <th>Email</th>
                        <th>Phân quyền</th>
                        <th>Trạng thái</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
