@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Chỉnh sửa')
{{-- @section('username', '????') --}}

@section('content')
<!-- Main content -->
<link rel="stylesheet" href="{{ asset('admin/management/book/css/style.css') }}">

<section class="content">
    <form method="POST" action="{{ route('admin.manage.book.update', ['id' => $data->idBook]) }}"  enctype="multipart/form-data">
    @csrf
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Thêm hình ảnh
                <span>
                    @if ($errors->has('image'))
                    <strong style="color: red; font-size: 80%">
                        {{ $errors->first('image') }}
                    </strong>
                    @endif
                </span>
              </h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
                <div class="img-container">
                    <input type="file" class="cl1" name="image" id="file" onchange="loadFile(event)" style="display: none">
                    <label for="file" class="cl1" style="cursor: pointer"><img src="{{ asset('admin/management/book/img/add.png') }}" class="resize"></label>

                    <img id="output" class="resize" alt="" src="{{ ($data->imageBook != null) ? asset('imgStorage/book/' . $data->imageBook) : '' }}"/>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thông tin
                <span>
                    @if ($errors->has('existingBook'))
                    <strong style="color: red; font-size: 80%">
                        {{ $errors->first('existingBook') }}
                    </strong>
                    @endif
                </span>
            </h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label for="inputName">Tên sách
                    <span>
                        @if ($errors->has('name'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('name') }}
                        </strong>
                        @endif
                    </span>
                </label>
                <input type="text" id="inputName" class="form-control" name="name" value="{{ $data->nameBook }}">
            </div>
            <div class="form-group">
                <label for="inputName">Tác giả
                    <span>
                        @if ($errors->has('author'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('author') }}
                        </strong>
                        @endif
                    </span>
                </label>
                <input type="text" id="inputName" class="form-control" name="author" value="{{ $data->nameAuthor }}">
            </div>
            <div class="form-group">
                <label>Thể loại</label>
                <div class="select2-primary">
                  <select name="categories[]" class="select2" multiple data-placeholder="Chọn" data-dropdown-css-class="select2-primary" style="width: 100%;">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"  {{ (in_array($category->id, $categorySelected)) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDescription">Mô tả</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description">{{ $data->descriptionBook }}</textarea>
                {{-- <script>CKEDITOR.replace('description')</script> --}}
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('admin.manage.book.index') }}" class="btn btn-secondary">Hủy</a>
        <button class="btn btn-success float-right" type="submit">Thực hiện</button>
      </div>
    </div>
    </form>
</section>

<script>
    var loadFile = function(event) {
        var image = document.getElementById('output');
        image.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
<!-- /.content -->
@endsection
