@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Thêm')
@section('additionButtonOpen', 'menu-open')
@section('additionButtonSelected', 'active')
@section('additionButtonBookSelected', 'active')
{{-- @section('username', '????') --}}

@section('content')
<!-- Main content -->
<link rel="stylesheet" href="{{ asset('admin/management/book/css/style.css') }}">

<section class="content">
    <form method="POST" action="{{ route('admin.manage.book.store.existing', ['id' => $data->idBook]) }}">
    @csrf
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Hình ảnh</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
                <div class="img-container">
                    <img id="output" class="resize" alt="" src="{{ ($data->imageBook != null) ? asset('imgStorage/book/' . $data->imageBook) : '' }}"/>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thông tin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label for="inputName">Tên sách</label>
                <input type="text" id="inputName" class="form-control" name="name" value="{{ $data->nameBook }}" disabled>
            </div>
            <div class="form-group">
                <label for="inputName">Tác giả</label>
                <input type="text" id="inputName" class="form-control" name="author" value="{{ $data->nameAuthor }}" disabled>
            </div>
            <div class="form-group">
                <label>Thể loại</label>
                <div class="select2-primary">
                  <select name="categories[]" class="select2" multiple data-placeholder="Chọn" data-dropdown-css-class="select2-primary" style="width: 100%;" disabled>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"  {{ (in_array($category->id, $categorySelected)) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputName">Thông số
                    <span>
                        @if ($errors->has('parameter'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('parameter') }}
                        </strong>
                        @endif
                    </span>
                    <span>
                        <p style="color: gray; font-size: 90%">(ngày xuất bản, đặc điểm, chi chú,... để phân biệt sách cùng loại)</p>
                    </span>
                </label>
                <textarea id="inputDescription" class="form-control" rows="2" name="parameter">{{ old('parameter') }}</textarea>
            </div>
            <div class="form-group">
                <label for="inputDescription">Mô tả</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description" disabled>{{ $data->descriptionBook }}</textarea>
                {{-- <script>CKEDITOR.replace('description')</script> --}}
            </div>
            <div class="form-group">
                <label for="inputName">Giá
                    <span>
                        @if ($errors->has('price'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('price') }}
                        </strong>
                        @endif
                    </span>
                </label>
                <input type="text" id="inputName" class="form-control" name="price" value="{{ old('price') }}">
            </div>
            <div class="form-group">
                <label for="inputStatus">Nhà xuất bản
                    <span>
                        @if ($errors->has('publishCompany'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('publishCompany') }}
                        </strong>
                        @endif
                    </span>
                </label>
                <select class="form-control custom-select" name="publishCompany">
                  <option selected disabled>Chọn</option>
                    @foreach ($publishCompanies as $publishCom)
                        <option value="{{ $publishCom->id }}">{{ $publishCom->name }}</option>
                    @endforeach
                </select>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('admin.manage.book.create') }}" class="btn btn-secondary">Hủy</a>
        <button class="btn btn-success float-right" type="submit">Thực hiện</button>
      </div>
    </div>
    </form>
</section>
<!-- /.content -->
@endsection
