@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Chọn cách thức tạo mới')
@section('additionButtonOpen', 'menu-open')
@section('additionButtonSelected', 'active')
@section('additionButtonBookSelected', 'active')
{{-- @section('username', '????') --}}

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header"><h3 class="card-title">Mới hoàn toàn</h3></div>
            <div class="card-body">
                <p>Tạo mới một đầu sách chưa từng được thêm vào thư viện</p>
                <p>Một cuốn sách có nội dung hoàn toàn mới.</p>
                <br><br><br><br><br>
                <a href="{{ route('admin.manage.book.create.new') }}"><button class="btn btn-success float-right" type="submit">Thực hiện</button></a>
            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header"><h3 class="card-title">Đã tồn tại</h3></div>
            <div class="card-body">
                <p>Tạo một
                    <span><strong>phiên bản khác</strong>
                    của đầu
                    <span><strong>sách đã tồn tại</strong></span>
                    trong thư viện
                </p>
                <p>Cùng một cuốn sách nhưng chỉ khác về:</p>
                <p>- Nhà xuất bản.</p>
                <p>- Ngày tái bản.</pnhà>
                <p>- Các thông tin nhận dạng khác.</p>
                <a href="{{ route('admin.manage.book.create.existingIndex') }}"><button class="btn btn-success float-right" type="submit">Thực hiện</button></a>
            </div>
        </div>
      </div>
    </div>
</section>
<!-- /.content -->
@endsection
