@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Chi tiết')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonBookSelected', 'active')

@section('content')
<!-- Main content -->
<link rel="stylesheet" href="{{ asset('admin/management/book/css/style.css') }}">

<section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Thông số</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
                {{-- <div class="form-group"> --}}
                    <h7>Tổng cộng:<span>asdsa</span>

                    </h7>

                {{-- </div> --}}




            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thông tin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label for="inputName">Tên sách</label>
                <input type="text" id="inputName" class="form-control" name="name" value="{{ $book->nameBook }}" disabled>
            </div>
            <div class="form-group">
                <label for="inputName">Tác giả</label>
                <input type="text" id="inputName" class="form-control" name="author" value="{{ $book->nameAuthor }}" disabled>
            </div>
            <div class="form-group">
                <label>Thể loại</label>
                <div class="select2-primary">
                  <select name="categories[]" class="select2" multiple data-placeholder="Chọn" data-dropdown-css-class="select2-primary" style="width: 100%;" disabled>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"  {{ (in_array($category->id, $categorySelected)) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDescription">Mô tả</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description" disabled>{{ $book->descriptionBook }}</textarea>
                {{-- <script>CKEDITOR.replace('description')</script> --}}
            </div>
            <div class="form-group">
                <label for="inputStatus">Nhà xuất bản</label>
                <select class="form-control custom-select" name="publishCompany">
                    <option selected disabled>{{ $payload->namePublishCompany }}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputDescription">Thông số</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="parameter" disabled>{{ $payload->parameterBook }}</textarea>
                {{-- <script>CKEDITOR.replace('description')</script> --}}
            </div>
            <div class="form-group">
                <label for="inputName">Giá</label>
                <input type="text" id="inputName" class="form-control" name="price" value="{{ $payload->priceBook }}" disabled>
            </div>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <a href="{{ route('admin.manage.book.edit', ['id' => $book->idBook]) }}">
                        <button type="button" class="btn btn-block btn-outline-secondary btn-lg">Sửa</button>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{ route('admin.manage.book.destroy', ['id' => $book->idBook]) }}">
                        <button onclick="return alert('Bạn có chắc  muốn xóa?')" type="button" class="btn btn-block btn-outline-danger btn-lg">Xóa</button>
                    </a>
                </div>
                <div class="col-md-2"></div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Danh sách sách</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Mã số</th>
                        <th>Trạng thái</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    {{-- @foreach ($childs as $item)
                    <tr>
                        <td>{{ $item->namePublishCompany }}</td>
                        <td>{{ $item->parameterBook }}</td>
                        <td>{{ $item->priceBook }}</td>
                        <td>
                            <a href="">
                                <button type="button" class="btn btn-block btn-outline-primary btn-lg">Thêm 1 quyển</button>
                            </a>
                        </td>
                        <td>
                            <a href="">
                                <button type="button" class="btn btn-block btn-outline-info btn-lg">Chi tiết</button>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.manage.book.destroyOneVersion', ['id' => $item->idPubBoo, 'idReturn' => $book->idBook]) }}">
                                <button onclick="return alert('Bạn có chắc muốn xóa?')" type="button" class="btn btn-block btn-outline-danger btn-lg">Xóa</button>
                            </a>
                        </td>
                    </tr>
                    @endforeach --}}

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Mã số</th>
                        <th>Trạng thái</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
              </div>
          </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
