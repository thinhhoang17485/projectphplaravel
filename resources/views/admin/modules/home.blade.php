@extends('admin/master')
@section('title', 'Trang chủ')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thành viên nhóm 1</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label for="inputName">Hoàng Ngọc Thịnh</label>
            </div>
            <div class="form-group">
                <label for="inputName">Võ Minh Dương</label>
            </div>
            <div class="form-group">
                <label for="inputName">Phan Gia Nhựt</label>
            </div>
            <div class="form-group">
                <label for="inputName">Nhâm Sỹ Nam</label>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="col-md-3"></div>
    </div>
</section>
<!-- /.content -->
@endsection
