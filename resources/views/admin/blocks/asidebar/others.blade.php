<li class="nav-header">Khác</li>
<li class="nav-item">
    <a href="{{ route('admin.others.request.index') }}" class="nav-link @yield('requestButtonSelected', '')">
       <i class="nav-icon fas fa-th"></i>
       <p>
          Yêu cầu
          <span class="right badge badge-danger">Mới</span>
       </p>
    </a>
 </li>
<li class="nav-item">
<a href="{{ route('admin.others.statistical.index') }}" class="nav-link @yield('statisticalButtonSelected', '')">
    <i class="nav-icon far fa-image"></i>
    <p>
        Thống kê
        <span class="badge badge-info right">2</span>
    </p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('admin.others.analysis.index') }}" class="nav-link @yield('analysisButtonSelected', '')">
    <i class="nav-icon far fa-plus-square"></i>
    <p>
        Phân tích
    </p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('admin.others.report.index') }}" class="nav-link @yield('reportButtonSelected', '')">
    <i class="nav-icon far fa-envelope"></i>
    <p>
        Phản hồi
    </p>
</a>
</li>
<li class="nav-item has-treeview @yield('historyButtonOpen', '')">
<a href="" class="nav-link @yield('historyButtonSelected', '')">
    <i class="nav-icon far fa-calendar-alt"></i>
    <p>
        Lịch sử
        <i class="fas fa-angle-left right"></i>
    </p>
</a>
<ul class="nav nav-treeview">
    <li class="nav-item">
        <a href="{{ route('admin.others.history.user') }}" class="nav-link @yield('historyButtonUserSelected', '')">
            <i class="far fa-circle nav-icon"></i>
            <p>Người dùng</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.others.history.search') }}" class="nav-link @yield('historyButtonSearchSelected', '')">
            <i class="far fa-circle nav-icon"></i>
            <p>Tìm kiếm</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.others.history.admin') }}" class="nav-link @yield('historyButtonAdminSelected', '')">
            <i class="far fa-circle nav-icon"></i>
            <p>Quản trị</p>
        </a>
    </li>
</ul>
</li>
