
<li class="nav-item has-treeview @yield('managementButtonOpen', '')">
    <a href="" class="nav-link @yield('managementButtonSelected', '')">
       <i class="nav-icon fas fa-tachometer-alt"></i>
       <p>
          Quản lí
          <i class="right fas fa-angle-left"></i>
       </p>
    </a>
    <ul class="nav nav-treeview">
       <li class="nav-item">
          <a href="{{ route('admin.manage.account.index') }}" class="nav-link @yield('managementButtonAccountSelected', '')">
             <i class="far fa-circle nav-icon"></i>
             <p>Tài khoản</p>
          </a>
       </li>
       <li class="nav-item">
          <a href="{{ route('admin.manage.book.index') }}" class="nav-link @yield('managementButtonBookSelected', '')">
             <i class="far fa-circle nav-icon"></i>
             <p>Sách</p>
          </a>
       </li>
       <li class="nav-item">
        <a href="{{ route('admin.manage.category.index') }}" class="nav-link @yield('managementButtonCategorySelected', '')">
           <i class="far fa-circle nav-icon"></i>
           <p>Thể loại</p>
        </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.manage.publishCompany.index') }}" class="nav-link @yield('managementButtonPublishComSelected', '')">
               <i class="far fa-circle nav-icon"></i>
               <p>Nhà xuất bản</p>
            </a>
         </li>
    </ul>
 </li>
 <li class="nav-item has-treeview @yield('additionButtonOpen', '')">
    <a href="" class="nav-link @yield('additionButtonSelected', '')">
       <i class="nav-icon fas fa-copy"></i>
       <p>
          Thêm
          <i class="fas fa-angle-left right"></i>
       </p>
    </a>
    <ul class="nav nav-treeview">
       <li class="nav-item">
          <a href="{{ route('admin.manage.account.create') }}" class="nav-link @yield('additionButtonAccountSelected', '')">
             <i class="far fa-circle nav-icon"></i>
             <p>Tài khoản</p>
          </a>
       </li>
       <li class="nav-item">
          <a href="{{ route('admin.manage.book.create') }}" class="nav-link @yield('additionButtonBookSelected', '')">
             <i class="far fa-circle nav-icon"></i>
             <p>Sách</p>
          </a>
       </li>
       <li class="nav-item">
          <a href="{{ route('admin.manage.category.create') }}" class="nav-link @yield('additionButtonCategorySelected', '')">
             <i class="far fa-circle nav-icon"></i>
             <p>Thể loại</p>
          </a>
       </li>
       <li class="nav-item">
          <a href="{{ route('admin.manage.publishCompany.create') }}" class="nav-link @yield('additionButtonPublishComSelected', '')">
             <i class="far fa-circle nav-icon"></i>
             <p>Nhà xuất bản</p>
          </a>
       </li>
    </ul>
 </li>
 <li class="nav-item">
    <a href="{{ route('admin.manage.profile.index') }}" class="nav-link @yield('profileButtonSelected', '')">
       <i class="nav-icon fas fa-book"></i>
       <p>
          Hồ sơ
       </p>
    </a>
 </li>
