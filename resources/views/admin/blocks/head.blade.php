<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title', 'Admin')</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/ionicons.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/dist/css/adminlte.min.css') }}">
<!-- Google Font: Source Sans Pro -->
<link href="{{ asset('admin/masterLayout/fonts.css') }}" rel="stylesheet">




<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/daterangepicker/daterangepicker.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('admin/masterLayout/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">


{{-- ck editor --}}
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
