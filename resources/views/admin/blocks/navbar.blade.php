
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    @include('admin/blocks/navbar/leftNavbar')
    <!-- SEARCH FORM -->
    @include('admin/blocks/navbar/search')
    <!-- Right navbar links -->
    @include('admin/blocks/navbar/rightNavbar')
 </nav>

