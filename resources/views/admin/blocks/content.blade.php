<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="container-fluid">
          <div class="row mb-2">
             <div class="col-sm-6">
                <h1>@yield('title', 'Admin')</h1>
             </div>
          </div>
       </div>
       <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
       <div class="container-fluid">
          <div class="row">
             <div class="col-12">
                <!-- Default box -->
                <div class="card">
                   <div class="card-header">
                      <h3 class="card-title">@yield('taskname', '')</h3>
                      <div class="card-tools">
                         <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                         <i class="fas fa-minus"></i></button>
                      </div>
                   </div>
                   <div class="card-body">
                      <!-- Main content -->
                      @yield('content')
                   </div>
                   <!-- /.card-body -->
                   <div class="card-footer">
                      Nhóm 1
                   </div>
                   <!-- /.card-footer-->
                </div>
                <!-- /.card -->
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
 </div>
